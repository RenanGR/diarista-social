class CreateTypeServices < ActiveRecord::Migration
  def change
    create_table :type_services do |t|
      t.string :descricao

      t.timestamps null: false
    end
  end
  def self.down
    drop_table :type_services
  end  
end
