class AddFkUsersServices < ActiveRecord::Migration
  def change
  	#add_foreign_key :user_services, :type_services, column: :id_tipo_servico
    #add_reference :user_services, :type_service, index: true, foreign_key: true
    #add_reference :user_services, :user, index: true, foreign_key: true

    add_reference :user_services, :user, index: true, foreign_key: true
    add_reference :user_services, :type_service , index: true, foreign_key: true
  end
  def self.down
  	remove_foreign_key :user_services, :type_services
  end	
end
