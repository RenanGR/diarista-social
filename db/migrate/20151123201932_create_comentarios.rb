class CreateComentarios < ActiveRecord::Migration
  def change
    create_table :comentarios do |t|
      t.string :descricao
      t.date :data
      t.references :service, index: true,foreign_key: true
      t.references :user ,index: true, foreign_key: true

      t.timestamps null: false
    end
   # add_foreign_key :comentarios, :users, column: :user_id_comentarios
   # add_foreign_key :comentarios, :services, column: :id_comentario_servico
  end

  def self.down
   	drop_table :comentarios
  end
end
