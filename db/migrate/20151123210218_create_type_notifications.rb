class CreateTypeNotifications < ActiveRecord::Migration
  def change
    create_table :type_notifications do |t|
      t.string :descricao

      t.timestamps null: false
    end
  end
  
  def self.down
  	drop_table :type_notifications
  end
end
