class CreateNotifications < ActiveRecord::Migration
  def change
    create_table :notifications do |t|
      t.string :descricao
      #t.references :type_notification, index: true,foreign_key: true


      t.timestamps null: false
    end
    # add_foreign_key :notifications, :tipo_notifications
  end

  def self.down
  	drop_table :notifications
  end
end
