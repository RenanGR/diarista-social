class AddFkServices < ActiveRecord::Migration
  def change
    add_reference :services, :user, index: true, foreign_key: true
    add_reference :services, :user_service, index: true, foreign_key: true


  end
  
  def self.down
  	remove_foreign_key :services, :user
  	remove_foreign_key :services, :user_service
  end	
end
