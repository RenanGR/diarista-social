class CreateRecomendacaos < ActiveRecord::Migration
  def change
    create_table :recomendacaos do |t|

      t.references :user_service, index: true,foreign_key: true
      t.references :user ,index: true, foreign_key: true

      t.timestamps null: false
    end

  end

  def self.down
  	drop_table :recomendacaos
  end
end
