class CreateServices < ActiveRecord::Migration
  def change
    create_table :services do |t|
      t.string :status
      t.integer :avaliacao
      t.string :tipo
      t.integer :id_tipo
      t.string :descricao
      t.date :data_servico
     # t.references :user_service, index: true,foreign_key: true
     # t.references :user ,index: true, foreign_key: true


      t.timestamps null: false


    end

    #add_foreign_key :services, :user, column: :user_id
    # add_foreign_key :services, :user_services, column: :id_user_type_service
    # add_foreign_key :services, :type_services, column: :id_tipo
  
  end

  def self.down
    drop_table :services
  end  
end
