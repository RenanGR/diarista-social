class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      ## autenticação do usuario		
      t.string :nome,              	null: false, default: ""
      t.string :email,				null: false, default: ""
      t.string :telefone,			null: false, default: ""
      t.string :encrypted_password,	null: false, default: ""
      t.string :estado,				null: false, default: ""
      t.string :cidade,             null: false, default: ""
      t.string :bairro,             null: false, default: ""
      t.string :rua,            	null: false, default: ""
      t.string :complemento,		null: false, default: ""
      t.string :descricao, 			null: false, default: ""
      t.string :numero, 			null: false, default: ""
      t.integer :cep,				null: false, default: 0
      t.string :img_perfil, 		null: false, default: ""

      ## recuperação
      t.string   :reset_password_token
      t.datetime :reset_password_sent_at


      t.timestamps 
    end


    add_index :users, :email,               :unique => true
    add_index :users, :encrypted_password,  :unique => true


  def self.down
    drop_table :users
  end  
  end
end
