# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151209040945) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "comentarios", force: :cascade do |t|
    t.string   "descricao"
    t.date     "data"
    t.integer  "service_id"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "comentarios", ["service_id"], name: "index_comentarios_on_service_id", using: :btree
  add_index "comentarios", ["user_id"], name: "index_comentarios_on_user_id", using: :btree

  create_table "notifications", force: :cascade do |t|
    t.string   "descricao"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.integer  "type_notification_id"
  end

  add_index "notifications", ["type_notification_id"], name: "index_notifications_on_type_notification_id", using: :btree

  create_table "recomendacaos", force: :cascade do |t|
    t.integer  "user_service_id"
    t.integer  "user_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "recomendacaos", ["user_id"], name: "index_recomendacaos_on_user_id", using: :btree
  add_index "recomendacaos", ["user_service_id"], name: "index_recomendacaos_on_user_service_id", using: :btree

  create_table "services", force: :cascade do |t|
    t.string   "status"
    t.integer  "avaliacao"
    t.string   "tipo"
    t.integer  "id_tipo"
    t.string   "descricao"
    t.date     "data_servico"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.integer  "user_id"
    t.integer  "user_service_id"
  end

  add_index "services", ["user_id"], name: "index_services_on_user_id", using: :btree
  add_index "services", ["user_service_id"], name: "index_services_on_user_service_id", using: :btree

  create_table "type_notifications", force: :cascade do |t|
    t.string   "descricao"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "type_services", force: :cascade do |t|
    t.string   "descricao"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "user_services", force: :cascade do |t|
    t.string   "descricao"
    t.boolean  "ativo"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.integer  "user_id"
    t.integer  "type_service_id"
  end

  add_index "user_services", ["type_service_id"], name: "index_user_services_on_type_service_id", using: :btree
  add_index "user_services", ["user_id"], name: "index_user_services_on_user_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "nome",                   default: "", null: false
    t.string   "email",                  default: "", null: false
    t.string   "telefone",               default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "estado",                 default: "", null: false
    t.string   "cidade",                 default: "", null: false
    t.string   "bairro",                 default: "", null: false
    t.string   "rua",                    default: "", null: false
    t.string   "complemento",            default: "", null: false
    t.string   "descricao",              default: "", null: false
    t.string   "numero",                 default: "", null: false
    t.integer  "cep",                    default: 0,  null: false
    t.string   "img_perfil",             default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["encrypted_password"], name: "index_users_on_encrypted_password", unique: true, using: :btree

  add_foreign_key "comentarios", "services"
  add_foreign_key "comentarios", "users"
  add_foreign_key "notifications", "type_notifications"
  add_foreign_key "recomendacaos", "user_services"
  add_foreign_key "recomendacaos", "users"
  add_foreign_key "services", "user_services"
  add_foreign_key "services", "users"
  add_foreign_key "user_services", "type_services"
  add_foreign_key "user_services", "users"
end
