class HomesController < ApplicationController


	before_action :require_no_authentication, only: [:index,:new,:create]
	before_action :require_authentication,only: :destroy


	def index
		@user_session=UserSession.new(session)
		@user=User.new
	end

	def show
		@@user=User.find(params[:id])
	end


	def new
		@user=User.new
	end


	def create
		@user = User.new(user_params)
		if @user.save

			@user_session=UserSession.new(session,params[:user])

			if @user_session.authenticate!

				redirect_to user_path(@user), :notice => 'Cadastro realizado com sucesso papai'
			else
				render :new ,:notice => 'algo deu errado'
			end

		end
	end

	private

	def user_params
		params.
				require(:user).permit( :email,:nome,:encrypted_password)

	end



end
