class UsersController < ApplicationController
  #before_action :can_change, only: [:edit,:update,:show]



  def new
    @user = User.new

  end

  def show
    @user=User.find(params[:id])


  end

  def edit
    @user=User.find(params[:id])
  end


  def create
    @user = User.new(user_params)
    if @user.save

      @user_session=UserSession.new(session,params[:user])

      if @user_session.authenticate!
        redirect_to home_path(@user), :notice => 'Cadastro realizado com sucessoqqqqq'

      end
    end
  end

  def update
    @user=User.find(params[:id])
    if @user.update(user_params)
      redirect_to root_path, notice: ('Login realizado com sucesso')
    else
      render action: :edit
    end
  end


  private

  def user_params
    params.
        require(:user).permit(:email,:encrypted_password)

  end


  def can_change
    unless user_signed_in? && current_user==user
      redirect_to user_path(session[:user_id]),notice: t("Ação não permitida")

    end

  end


  def user
    @user ||=User.find(params[:id])

  end
end
