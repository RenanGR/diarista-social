
class ServicesController < ApplicationController


  @user_service


  def seta_user_service

    @user_service=UserService.find_by_id(params[:id])

    redirect_to new_service_path

  end

  def index

    @user_services=UserService.all
    @user_service=current_user.user_services.build()

  end

  def show


    
  end

  def new
    @service=current_user.services.build
    
  end

  def create
    
  end

  def tudo

    @user_service=current_user.user_services.build(user_service_params)
    if @user_service.save
      redirect_to user_services_path, :notice => 'Cadastro de servico realizado com sucessp'
    else
      render :index
    end

  end

  def edit
    
  end

  def update
    
  end

  def destroy

  end

  private
  def user_service_params
    params.
        require(:user_service).permit(:descricao,:type_service_id)

  end
end