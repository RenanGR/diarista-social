class UserServicesController < ApplicationController


  before_action :require_authentication,only:[:new,:create]

  def index

    @user_services=current_user.user_services.all

  end

  def new
    @user_service=current_user.user_services.build
  end

  def show
    @user_services=UserService.all
  end

  def create
    @user_service=current_user.user_services.build(user_service_params)
    if @user_service.save
      redirect_to user_services_path, :notice => 'Cadastro de servico realizado com sucessp'
    else
      render services_path, notice: ('Campos vazios')
    end

  end

  def edit

  end

  def destroy

  end


  def salva

  end

private
  def user_service_params
    params.
        require(:user_service).permit(:descricao,:type_service_id)

  end

end