class ApplicationController < ActionController::Base

  delegate :current_user, :user_signed_in?, to: :user_session

  helper_method :current_user, :user_signed_in?

  def user_session
    UserSession.new(session)
  end

  def require_authentication
    unless user_signed_in?
      redirect_to homes_path, alert: t('VC precisa estar logado')
    end

  end

  def require_no_authentication
    if user_signed_in?
      redirect_to user_path(current_user),notice: t('Vc já estar logado')

    end
  end


end
