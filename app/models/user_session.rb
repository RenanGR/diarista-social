class UserSession
  include ActiveModel::Model

  attr_accessor :email, :password

  def initialize(session,attribures={})

    @session=session
    @email=attribures[:email]
    @encrypted_password=attribures[:encrypted_password]
  end

  def authenticate!
    user=User.authenticate(@email,@encrypted_password)
    if user.present?
      store(user)
    else
      errors.add(:base, "Senha ou usuário invalid")

      false
    end

  end

  def store(user)
    if user.present?
      @session[:user_id]=user.id
    end
  end


  def current_user
    if @session[:user_id]==nil
      return nil
    else
      return User.find_by_id(@session[:user_id])
    end

  end

  def user_signed_in?
    @session[:user_id].present?

  end

  def destroy
    @session[:user_id]=nil
  end

end
