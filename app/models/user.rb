class User < ActiveRecord::Base
	has_many :user_services
	has_many :services
	has_many :comentarios
	has_many :notifications

	validates_presence_of :email,:nome,:encrypted_password


	def self.authenticate(email,password)
		user=User.where(["email=:email",{email:email}]).first
		return user

	end

end
